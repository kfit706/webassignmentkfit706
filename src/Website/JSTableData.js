var jobs_json = [

    //This is the array that stores the table data.
    {
        "role": "Role",
        "seek_jobs": "Jobs on Seek",
        "salary_max": "Max Salary",
        "salary_min": "Min Salary",
        "skills_knowledge": "Skills and Knowledge",
        "description": "Job Description"
        "image": < img src = "Images/SPDAmy.jpg" height = "60" width = "60" >
},
{
    "role"
:
    "Software Developers & Programmers",
        "seek_jobs"
:
    "590",
        "salary_max"
:
    "100",
        "salary_min"
:
    "72",
        "skills_knowledge"
:
    ["computer software and systems" + "\n", "programming languages and techniques" + "\n", "software development processes such as Agile" + "\n", "confidentiality, data security and data protection issues"],
        "description"
:
    "Software developers and programmers develop and maintain computer software, websites and software applications (apps)."
}
,
{
    "role"
:
    "Database & Systems Administrators",
        "seek_jobs"
:
    "74",
        "salary_max"
:
    "90",
        "salary_min"
:
    "66",
        "skills_knowledge"
:
    ["a range of database technologies and operating systems" + "\n", "new developments in databases and security systems" + "\n", "computer and database principles and protocols"],
        "description"
:
    "Database & systems administrators develop, maintain and administer computer operating systems, database management systems, and security policies and procedures."
}
,
{
    "role"
:
    "Help Desk & IT Support",
        "seek_jobs"
:
    "143",
        "salary_max"
:
    "65",
        "salary_min"
:
    "46",
        "skills_knowledge"
:
    ["computer hardware, software, networks and websites" + "\n", "the latest developments in information technology"],
        "description"
:
    "Information technology (IT) helpdesk/support technicians set up computer and other IT equipment and help prevent, identify and fix problems with IT hardware and software."
}
,
{
    "role"
:
    "Data Analyst",
        "seek_jobs"
:
    "270",
        "salary_max"
:
    "128",
        "salary_min"
:
    "69",
        "skills_knowledge"
:
    ["data analysis tools such as Excel, SQL, SAP and Oracle, SAS or R" + "\n", "data analysis, mapping and modelling techniques" + "\n", "analytical techniques such as data mining"],
        "description"
:
    "Data analysts identify and communicate trends in data using statistics and specialised software to help organisations achieve their business aims."
}
,
{
    "role"
:
    "Test Analyst",
        "seek_jobs"
:
    "127",
        "salary_max"
:
    "98",
        "salary_min"
:
    "70",
        "skills_knowledge"
:
    ["programming methods and technology" + "\n", "computer software and systems" + "\n", "project management"],
        "description"
:
    "Test analysts design and carry out testing processes for new and upgraded computer software and systems, analyse the results, and identify and report problems."
}
,
{
    "role"
:
    "Project Management",
        "seek_jobs"
:
    "188",
        "salary_max"
:
    "190",
        "salary_min"
:
    "110",
        "skills_knowledge"
:
    ["principles of project management" + "\n", "approaches and techniques such as Kanban and continuous testing" + "\n", "how to handle software development issues" + "\n", "common web technologies used by the scrum team"],
        "description"
:
    "Project managers use various methods to keep project teams on track. They also help remove obstacles to progress."
}
]
;

//This is the function which creates the table from the table data array above
$(document).ready(function createTable() {

            var jobs_table = $('<div>');
            jobs_table.addClass('jobs');
            var table = $('<table>');
            jobs_table.addClass('table');

            for (var h = 0; h < jobs_json.length; h++) {
            var table_row = $('<tr>');
            jobs_table.addClass('row');
            for (var i = 0; i < jobs_json.length; i++) {
            var role = $('<td>').text(jobs_json[h].role);
            jobs_table.addClass('rowData');
            var seek_jobs = $('<td>').text(jobs_json[h].seek_jobs);
            jobs_table.addClass('rowData');
            var salary_max = $('<td>').text(jobs_json[h].salary_max);
            jobs_table.addClass('rowData');
            var salary_min = $('<td>').text(jobs_json[h].salary_min);
            jobs_table.addClass('rowData');
            var skills_knowledge = $('<td>').text(jobs_json[h].skills_knowledge);
            jobs_table.addClass('rowData');
            var description = $('<td>').text(jobs_json[h].description);
            jobs_table.addClass('rowData');
        }
            for (var j = 0; j < jobs_json.length; j++) {
            table_row.append(role);
            table_row.append(seek_jobs);
            table_row.append(salary_max);
            table_row.append(salary_min);
            //table_row.append(skills_knowledge);
            //table_row.append(description);
            table.append(table_row);
        }
            jobs_table.append(table);
            $('#JobTable').append(jobs_table);
        }
        }
            var tr = document.getElementsByTagName("tr");

            for (var m = 1; m < tr.length; m++) {
            tr[1].onclick = function () {
                //$('#JobInfo').exists === false;
                var job_description_table = $('<div>');
                job_description_table.addClass('descriptions');
                var table = $('<table>');
                table.addClass('table');
                var table_row = $('<tr>');
                jobs_table.addClass('row');
                var skills_knowledge = $('<td>').text(jobs_json[h].skills_knowledge);
                jobs_table.addClass('rowData');
                var description = $('<td>').text(jobs_json[h].description);
                jobs_table.addClass('rowData');
                var row_img = $('<td>').image((jobs_json[1].image));
                table.addClass('imageData');
                var img_row = $('<tr>');
                jobs_table.addClass('imageRow');
                var desc_row = $('<tr>');
                jobs_table.addClass('descRow');
                var skills_row = $('<tr>');
                jobs_table.addClass('skillsRow');
                var role = $('<td>').text(jobs_json[1].role);
                jobs_table.addClass('rowData');

                skills_row.append(skills_knowledge)
                table.append(skills_row)
                desc_row.append(role), (description)
                table.append(desc_row)
                big_row.append(row_img)
                table.append(img_row)
                table_row.append(role)
                table.append(table_row)
                job_description_table.append(table)
                $('#JobInfo').append(job_description_table);


            }
        }
        //this.style.fontWeight = "bold";
        var more_info = $('<div>');
        more_info.addClass('moreinfo');
        var head = $('<h2>').text(jobs_json[1].role) + 'Job Description:';
        head.addClass('job');
        var image = ('<img src="Images/SPDAmy.jpg" height="60" width="60">');
        image.addClass('image');


        $(more_info).append(head);
        $('#JobInfo').append(more_info);

    }
}
{
    tr[2].onclick = function () {
        //this.style.fontWeight = "bold";
        var more_info = $('<div>');
        more_info.addClass('moreinfo');
        var head = $('<h2>').text(jobs_json[2].role);
        head.addClass('job');
        more_info.append(head);
        $('#JobInfo').append(more_info);
    }
}
})
;










